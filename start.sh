#!/bin/bash
# activation de l'aenvironnement virtuel
source ./video_env/bin/activate
# mise à jour de youtube_dl avant de lancer l'appli.
if [[ $1 = "--update" ]];
then
  pip install -U pip youtube_dl
fi
python app/main.py
