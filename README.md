# Simple application tkinter de téléchargement de vidéos youtube.

Comme nous n'avons que Linux (Mint Ubuntu et Debian) à la maison, impossible d'affirmer que ça marche sous Macosx et Windows bien que cette application soit multi plate-forme (python et tkinter). C'est son installation sur ces plate-formes qui est à étudier.

vlc est requis comme lecteur des vidéos.

![videothèque](videotheque.png)

## Installation

Cloner le projet

```git clone git@framagit.org:tazogil/videotheque.git```

Un script installe ce qu'il faut :

``$ bash install_linux.sh``

Ce que fait ce script :

- Installe virtualenv et python3-pip s'ils sont absents. virtualenv créé des environnements virtuels pour les programmes python, sortes de boites étanches. pip est l'installeur python.
- Créé l'environnement virtuel 'video_env' dans le répertoire de l'application.
- Démarre l'environnement virtuel.
- pip installe youtube_dl.
- Créé l'icône de lancement Ubuntu.

## Utilisation

Dans un terminal :

```$ ./start.sh```

```$ ./start.sh --update```

L'option update récupère la dernière version de youtube_dl avant de lancer l'application. Utiliser cette option quand un téléchargement ne marche pas.

Il y sinon un joli icône (videotheque.desktop) pour Ubuntu.
