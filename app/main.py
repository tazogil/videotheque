import tkinter as tk
from tkinter import messagebox
from form import Form
import gettext
import settings as s


_ = gettext.gettext


class VideothequeMain():

    def __init__(self, master):
        self.master = master
        self.master.geometry("800x600")
        self.master.title(s.PROJECT_NAME)
        self.master.grid_columnconfigure(0, weight=1)
        self.master.grid_rowconfigure(0, weight=1)
        self.master.grid()
        self.form = Form(self.master)


def close_window():
    if messagebox.askokcancel(_("Fin"), _("QUITTER ?")):
        root.destroy()

root = tk.Tk()
root.protocol("WM_DELETE_WINDOW", close_window)
VideothequeMain(root)
root.mainloop()
