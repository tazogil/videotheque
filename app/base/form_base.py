import tkinter as tk
# from PIL import Image, ImageTk
from tkinter import ttk
import gettext
from os.path import join, abspath, dirname, isfile

# fr = gettext.translation('messages', localedir='locale', languages=['fr'])
# _ = fr.gettext
_ = gettext.gettext

class FormBase:

    def __init__(self, master):
        self.master = master
        self.basedir = dirname(abspath(__file__))
        style = ttk.Style()


        self. var_progress = tk.IntVar()

        self.frame_video = ttk.Frame(master)
        self.treeview_list = ttk.Treeview(self.frame_video)
        self.treeview_list.heading("#0", text="Classement")

        self.label_progress = ttk.Label(self.frame_video)
        self.button_start = ttk.Button(self.frame_video, command=self.start_command,text=_("Démarrer"))
        self.entry_url = ttk.Entry(self.frame_video)
        self.label_url = ttk.Label(self.frame_video, text=_("Colle ici l'URL de la vidéo youtube et appuie sur 'démarrer' (banane)"))
        self.progress = ttk.Progressbar(self.frame_video, mode=_("determinate"),variable=self. var_progress)
        self.entry_search = ttk.Entry(self.frame_video)
        self.entry_search.bind("<Return>", self.entry_search_return)


        self.frame_video.grid(column=0,padx=10,sticky="wens",row=0,pady=10)
        self.frame_video.grid_columnconfigure(0, weight=4, uniform="a")
        self.frame_video.grid_columnconfigure(1, weight=1, uniform="a")
        self.frame_video.grid_rowconfigure(0, weight=1, uniform="a")
        self.frame_video.grid_rowconfigure(1, weight=1, uniform="a")
        self.frame_video.grid_rowconfigure(2, weight=1, uniform="a")
        self.frame_video.grid_rowconfigure(3, weight=1, uniform="a")
        self.frame_video.grid_rowconfigure(4, weight=8, uniform="a")
        self.treeview_list.grid(padx=5,pady=10,row=4,column=0,sticky="wens",columnspan=2)
        self.label_progress.grid(column=0,padx=5,sticky="wens",row=3,pady=10)
        self.button_start.grid(column=1,padx=15,sticky="wens",row=1,pady=10)
        self.entry_url.grid(column=0,padx=5,sticky="wens",row=1,pady=10)
        self.label_url.grid(column=0,padx=5,sticky="wens",row=0,pady=10)
        self.progress.grid(padx=5,pady=10,row=2,column=0,sticky="wens",columnspan=2)
        self.entry_search.grid(column=1,padx=5,sticky="wens",row=3,pady=10)


    def start_command(self):
        pass

    def entry_search_return(self, evt):
       pass


