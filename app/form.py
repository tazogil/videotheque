from base.form_base import FormBase
from datetime import date, datetime
from os import walk
from os.path import splitext, getmtime, join, getsize, exists, basename
from subprocess import run
from tkinter import messagebox
from tkinter.filedialog import askdirectory
import gettext
import humanize
import locale
import json
import re
import settings as s
import tkinter as tk
import youtube_dl

_ = gettext.gettext

locale.setlocale(locale.LC_ALL, '')


class Form(FormBase):
    """
    Hérite de app.base.form_base.FormBase générée depuis app/base/form.yml
    """

    def __init__(self, master):
        super().__init__(master)
        self.source = None
        self.data = list()
        self.set_menu()
        self.set_treeview_list()
        self.load_source()
        if self.source is not None:
            self.update_treeview_list_from_source()
        self.entry_search.insert(0, "rechercher...")

    def set_menu(self):
        "Défini le menu"
        menubar = tk.Menu(self.master)
        filemenu = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label=_("Fichier"), menu=filemenu)
        filemenu.add_command(label=_("Source"),
                             command=self.load_videos)
        filemenu.add_command(label=_("Quitter"),
                             command=self.quit)
        self.master.config(menu=menubar)

    def set_treeview_list(self):
        "Défini les colonnes du treeview"
        cols = [
                ("date", "Date", 2),
                ("size", "Taille", 2),
                ("new", "Nouveaux", 2),
                ("timestamp", "Timestamp", 0),
                ("file", "File", 0),
                ("fullsize", "fullsize", 0)
                ]
        self.treeview_list["columns"] = [col[0] for col in cols]
        cols = cols[:-3]
        self.treeview_list["displaycolumns"] = [col[0] for col in cols]
        for (col, tit, l) in cols:
            self.treeview_list.column(col, width=l, stretch=True)
            self.treeview_list.heading(
                col, text=tit,
                command=lambda _col=col: self.treeview_list_sort(_col, False))
        self.treeview_list.heading("#0", text="Titre")
        self.treeview_list.column("#0", width=100)
        self.treeview_list.bind("<<TreeviewSelect>>", self.treeview_list_select)

    def load_videos(self):
        "Peuple le treeview avec le contenu du répertoire sélectionné?"
        "Action : menu->Fichier->Source"
        directory = askdirectory(
            title="Sélectionne le dossier contenant les vidéos")
        if directory:
            self.source = directory
            self.update_treeview_list_from_source()

    def update_treeview_list_from_source(self):
        "Lit le répertoire défini par self.source et rempli le treeview."
        self.treeview_list.delete(*self.treeview_list.get_children())
        self.data = list()
        for d, dl, fl in walk(self.source):
            for f in fl:
                filepath = join(d, f)
                filename, ext = splitext(f)
                now = datetime.now()
                new = now.year - 1
                res = re.findall("_\d{4}-\d{2}", filename)
                if len(res) == 1:
                    new = res[0][1:]
                timestamp = getmtime(filepath)
                odate = date.fromtimestamp(timestamp)
                strdate = odate.strftime("%A %d %B %Y")
                size = getsize(filepath)
                hsize = humanize.naturalsize(size, gnu=True)
                values = (strdate, hsize, new, timestamp, filepath, size)
                self.data.append((filename,)+values)
                self.treeview_list.insert('', 'end',
                                          text=filename, values=values)

    def treeview_list_sort(self, col, reverse):
        "Tri des colonnes du treeview"
        if col == "date":
            lst = [(self.treeview_list.set(k, "timestamp"), k)
                   for k in self.treeview_list.get_children('')]
        elif col == "size":
            lst = [(self.treeview_list.set(k, "fullsize"), k)
                   for k in self.treeview_list.get_children('')]
        else:
            lst = [(self.treeview_list.set(k, col), k)
                   for k in self.treeview_list.get_children('')]
        if col == "size":
            lst.sort(reverse=reverse, key=lambda lst: int(lst[0]))
        else:
            lst.sort(reverse=reverse, key=lambda lst: lst[0])
        for index, (val, k) in enumerate(lst):
            self.treeview_list.move(k, '', index)
        self.treeview_list.heading(col,
                                   command=lambda: self.treeview_list_sort(
                                       col, not reverse))

    def treeview_list_select(self, evt):
        "Ouvre la vidéo sélectionnée dans vlc."
        items = self.treeview_list.selection()
        item = items[0]
        (d, s, n, t, filepath, z) = self.treeview_list.item(item, "values")
        command = "vlc  \"{}\"".format(filepath)
        run(command, shell=True)

    def quit(self):
        """Proposer d'enregistrer le répertoire de téléchargement
        avant de quitter."""
        confirm = messagebox.askyesnocancel(
            title=_("Quitter"),
            message=_("Enregistrer l'emplacement "
                      "des vidéos avant de quitter ?"),
            default=messagebox.YES
        )
        if confirm is None:
            return
        with open(s.SOURCE_FILE, "w") as fh:
            source = {"source": self.source}
            json.dump(source, fh)
        self.master.destroy()

    def load_source(self):
        "Défini le répertoire de téléchargement dans self.source"
        with open(s.SOURCE_FILE, "r") as fh:
            source = json.load(fh)
            so = source.get("source", None)
            if so is not None:
                self.source = so
            else:
                messagebox.showwarning("Source indéfinie",
                                       "Aller dans me menu Fichier -> Source"
                                       " et choisissez le répertoire"
                                       " de téléchargement.")

    def entry_search_return(self, evt):
        "Recherche"
        searched = self.entry_search.get()
        if searched == "":
            self.update_treeview_list_from_source()
        else:
            founded = [(values[0], values[1:])
                       for values in self.data
                       if searched.lower() in values[0].lower()]
            self.treeview_list.delete(*self.treeview_list.get_children())
            for filename, values in founded:
                self.treeview_list.insert('', 'end',
                                          text=filename, values=values)

    def has_been_downloaded(self, url):
        "La vidéo url a déja été téléchargée."
        if not exists(s.DOWNLOAD_ARCHIVE):
            return
        with youtube_dl.YoutubeDL() as ydl:
            res = ydl.extract_info(url, download=False)
            id = res["id"]
            with open(s.DOWNLOAD_ARCHIVE, "r") as fh:
                ids = fh.read()
                if id in ids:
                    return True

    def start_command(self):
        """Lance un téléchargement.
        Ne prend pas les playlists
        """
        url = self.entry_url.get()
        if url == '':
            messagebox.showerror("Erreur", "Il manque l'URL !")
            return
        r = "&list="
        if r in url:
            url = url[:url.index(r)]

        if self.has_been_downloaded(url):
            messagebox.showwarning("Doublon !",
                                   "Cette vidéo a déja été téléchargée.")
            return
        dt = datetime.now()
        new = dt.strftime("%Y-%m")
        dl_opts = {'progress_hooks': [self.progress_download],
                   'noplaylist': True,
                   'download_archive': s.DOWNLOAD_ARCHIVE,
                   'outtmpl': "{0}/%(title)s-%(id)s_{1}.%(ext)s".format(
                       self.source, new)}
        with youtube_dl.YoutubeDL(dl_opts) as ydl:
            ydl.download([url])

    def progress_download(self, d):
        "Progression du téléchargement."
        if d["status"] == "downloading":
            fragment = "Téléchargement"
            frag_index = d.get("frag_index", None)
            if frag_index is not None:
                fragment = "Téléchargement de la piste vidéo"
                if frag_index == 1:
                    fragment = "Téléchargement  de la piste audio"
            prc_str = d["_percent_str"]
            prc_str = prc_str[:-1]
            prc_f = float(prc_str)
            self.var_progress.set(prc_f)
            self.progress.update_idletasks()
            tb = d.get("_total_bytes_estimate_str", None)
            if tb is None:
                tb = d.get("_total_bytes_str", None)
            self.label_progress.config(
                text="{0} {1} {2} fin dans {3} ({4})".format(
                    fragment,
                    d["_percent_str"],
                    tb,
                    d["_eta_str"],
                    d["_speed_str"]))
        elif d["status"] == "finished":
            self.tmpfilename = d["filename"]
            self.label_progress.config(
                text="Téléchargement terminé. Fusion vidéo/audio en cours...")
            self.wait_merging()

    def wait_merging(self):
        "Attente de la fusion des pistes vidéo et audio"
        if exists(self.tmpfilename):
            self.master.after(500, self.wait_merging)
        else:
            f = basename(self.tmpfilename)
            n, e = splitext(f)
            self.label_progress.config(text="{0} est prête".format(n))
            self.update_treeview_list_from_source()
