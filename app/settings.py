from os.path import abspath, join, dirname
PROJECT_NAME = "videotheque"
APP_DIR = dirname(abspath(__file__))
SOURCE_FILE = join(APP_DIR, "source.json")
DOWNLOAD_ARCHIVE = join(APP_DIR, ".download_archive")
