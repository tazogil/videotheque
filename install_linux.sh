#! /bin/bash
# Installation de l'appli vidéothèque

PWD="$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
V_ENV_NAME="video_env"
V_ENV="${PWD}${V_ENV_NAME}/"
PYTHON_EXE="${V_ENV}bin/python3"
IS_VIRTUALENV=`dpkg -l virtualenv | grep virtualenv | cut -c1,2`
test $IS_VIRTUALENV = "ii"
if [ $? -ne 0 ]; then
  echo "virtualenv est requis."
  sudo apt install virtualenv
fi
IS_PIP=`dpkg -l python3-pip | grep python3-pip | cut -c1,2`
test $IS_PIP = "ii"
if [ $? -ne 0 ]; then
  echo "python3-pip est requis."
  sudo apt install python3-pip
fi

echo "Création de l'environnement virtuel dans $V_ENV"
virtualenv -p /usr/bin/python3 video_env 
source "./${V_ENV_NAME}/bin/activate"
echo "Installation des librairies"
pip install -U pip
pip install -r "${PWD}requirements.txt"
chmod +x start.sh
echo "Création de l'icône de lancement"
echo "[Desktop Entry]" > videotheque.desktop
echo "Name=Videotheque" >> videotheque.desktop
echo "Exec=bash start.sh %F" >> videotheque.desktop
echo "Path=$PWD" >> videotheque.desktop
echo "Icon=${PWD}gnome-multimedia.png" >> videotheque.desktop
echo "Terminal=false" >> videotheque.desktop
echo "Type=Application" >> videotheque.desktop
echo "Fin de l'installation. Cliquez sur videotheque.desktop pour lancer l'application"




